export default {
	ignoreFiles: [
		'README.md',
		'_images/icon.png',
		'web-ext-artifacts',
		'web-ext-config.mjs',
		'!node_modules',
		'!node_modules/*',
		'!node_modules/*/LICENSE',
		'!node_modules/*/dist',
		'!node_modules/*/dist/*.min.js'
	],

	build: {
		overwriteDest: true,
	},
};
