// Related Bookmarks v0.3.2
// Copyright © 2024 Boian Berberov
//
// Licensed under the EUPL-1.2 only.
// License text: https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
// SPDX-License-Identifier: EUPL-1.2

'use strict';

let options_local;

let btn_treemap;
let btn_reset;
let cbx_log_lookups_count;
let cbx_log_related_lookups;
let sel_treetop;
let sel_tiling;
let sel_sort;
let sel_colors;
let sel_fontHeight;
let sel_paddingAround;
let sel_paddingBetween;

function update_all() {
	cbx_log_lookups_count.checked   = options_local.debug.log_lookups_count;
	cbx_log_related_lookups.checked = options_local.debug.log_related_lookups;
	sel_treetop.value               = options_local.treemap.treetop;
	sel_tiling.value                = options_local.treemap.tiling;
	sel_sort.value                  = options_local.treemap.sort;
	sel_colors.value                = options_local.treemap.colors;
	sel_fontHeight.value            = options_local.treemap.fontHeight;
	sel_paddingAround.value         = options_local.treemap.paddingAround;
	sel_paddingBetween.value        = options_local.treemap.paddingBetween;
}

window.addEventListener(
	'load',
	async function (event) {
		//
		// Initialize
		//

		// Load options
		options_local = await browser.storage.local.get(options_default);

		// Get DOM objects
		btn_treemap             = document.getElementById('btn_treemap');
		btn_reset               = document.getElementById('btn_reset');
		cbx_log_lookups_count   = document.getElementById('cbx_log_lookups_count');
		cbx_log_related_lookups = document.getElementById('cbx_log_related_lookups');
		sel_treetop             = document.getElementById('sel_treetop');
		sel_tiling              = document.getElementById('sel_tiling');
		sel_sort                = document.getElementById('sel_sort');
		sel_colors              = document.getElementById('sel_colors');
		sel_fontHeight          = document.getElementById('sel_fontHeight');
		sel_paddingAround       = document.getElementById('sel_paddingAround');
		sel_paddingBetween      = document.getElementById('sel_paddingBetween');

		update_all();

		d3.select(btn_treemap)
			.on(
				'click',
				async function(event) {
					const pageURL = browser.runtime.getURL('treemap/index.html');

					await browser.windows.create(
						{
							url:    pageURL,
							type:   'popup',
							height: 480,
							width:  800,
						}
					);
				}
			)
		;

		d3.select(btn_reset)
			.on(
				'click',
				async function(event) {
					// Reset options
					await browser.storage.local.set(options_default);

					// Reoad options
					options_local = await browser.storage.local.get();

					// Update GUI
					update_all();
				}
			)
		;

		d3.select(cbx_log_lookups_count)
			.on(
				'change',
				async function(event) {
					options_local.debug.log_lookups_count = this.value;
					await browser.storage.local.set(options_local);
				}
			)
		;

		d3.select(cbx_log_related_lookups)
			.on(
				'change',
				async function(event) {
					options_local.debug.log_related_lookups = this.value;
					await browser.storage.local.set(options_local);
				}
			)
		;

		d3.select(sel_treetop)
			.on(
				'change',
				async function(event) {
					options_local.treemap.treetop = this.value;
					await browser.storage.local.set(options_local);
				}
			)
		;

		d3.select(sel_tiling)
			.on(
				'change',
				async function(event) {
					options_local.treemap.tiling = this.value;
					await browser.storage.local.set(options_local);
				}
			)
		;

		d3.select(sel_sort)
			.on(
				'change',
				async function(event) {
					options_local.treemap.sort = this.value;
					await browser.storage.local.set(options_local);
				}
			)
		;

		d3.select(sel_colors)
			.on(
				'change',
				async function(event) {
					options_local.treemap.colors = this.value;
					await browser.storage.local.set(options_local);
				}
			)
		;

		d3.select(sel_fontHeight)
			.on(
				'change',
				async function(event) {
					options_local.treemap.fontHeight = this.value;
					await browser.storage.local.set(options_local);
				}
			)
		;

		d3.select(sel_paddingAround)
			.on(
				'change',
				async function(event) {
					options_local.treemap.paddingAround = this.value;
					await browser.storage.local.set(options_local);
				}
			)
		;

		d3.select(sel_paddingBetween)
			.on(
				'change',
				async function(event) {
					options_local.treemap.paddingBetween = this.value;
					await browser.storage.local.set(options_local);
				}
			)
		;
	}
);
