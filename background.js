// Related Bookmarks v0.3.2
// Copyright © 2019–2024 Boian Berberov
//
// Licensed under the EUPL-1.2 only.
// License text: https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
// SPDX-License-Identifier: EUPL-1.2

'use strict';

// Shared data
var tab_data = new Map();
let options_local;

// Internal data
const dup_color = '#ff0039';
const alt_color = '#ffe900';
const age_color = [0,0,128,128];
const index_names = [
	'Default.aspx',
	'default.asp',
	'default.aspx',
	'default.htm',
	'default.html',
	'index.cfm',
	'index.htm',
	'index.html',
	'index.php',
	'index.shtml',
	'index.xhtml',
];
const reserved_TLDs = [
	'example',
	'internal',
	'invalid',
	'local',
	'localdomain',
	'localhost',
	'test',
];

async function load_options() {
	// Load options
	options_local = await browser.storage.local.get(options_default);
}

//
// Tooltip generating functions
//

function tooltip_bookmark_single(bookmark) {
	return (
`Title:\u2002${bookmark.title}

Added:\u2002${ ( new Date(bookmark.dateAdded) ).toDateString() }

${tooltip_folder(bookmark)}`
	);
}

function tooltip_bookmark_multiple(bookmark) {
	return (
`\u25b6\u2003Title:\u2002${bookmark.title}
\u2003\u2003Added:\u2002${ ( new Date(bookmark.dateAdded) ).toDateString() }
${tooltip_folder(bookmark, '\u2003\u2003')}`
	);
}

function tooltip_bookmark_related(location) {
	return (
`${location.bookmarks.length}\u2002:\u2002${location.url.href}\u2002:\u2002${location.visits.length} visits`
	);
}

function tooltip_folder(bookmark, indent='') {
	let printed_path = indent + 'Folder:\n' + indent + `${bookmark.path[0].title}`;
	for (let i = 1; i < bookmark.path.length; i++)
	{
		printed_path += '\n' + indent + '\u2003'.repeat(i-1) + '\u2514\u2002' + bookmark.path[i].title;
	}
	return printed_path;
}

function tooltip_tags(bookmark) {
	return (
'Tags:\u2002not implemented by Mozilla'
	);
}

function tooltip_visits(visits) {
	// Must be called with (1 < visits.length) !
	return (
`Last Visited:\u2002${ ( new Date( visits[1].visitTime ) ).toDateString() }\u2003Visits:\u2002${(visits.length - 1)}`
	);
}

//
// Tree Of Responsibility functions
//

function TreeOfResponsibility(url, respSeq) {
	const answers = new Array();

	for (let i=0; i < respSeq.length; i++)
	{
		answers.push(
			respSeq[i]( url, respSeq.slice(i+1) )
		);
	}
	return answers.flat();
}

function alternativeProtocol(url, respSeq) {
	const query_list = new Array();
	const query_url  = new URL(url);

	const test_http  = (url.protocol === 'http:');
	const test_https = (url.protocol === 'https:');

	if (test_https || test_http)
	{
		// Input uses either HTTP or HTTPS protocols
		if (test_https)
		{
			// Swap HTTPS with HTTP
			query_url.protocol = 'http:';
		}
		else if (test_http)
		{
			// Swap HTTP with HTTPS
			query_url.protocol = 'https:';
		}

		// Append
		query_list.push(query_url);
		return query_list.concat(
			TreeOfResponsibility( query_url, respSeq )
		);
	}
	else
	{
		// Input does not use HTTP or HTTPS protocols

		// Skip any other protocols
		return query_list;
	}
}

function alternativeHostname(url, respSeq) {
	const query_list = new Array();
	const query_url  = new URL(url);

	const query_domain = url.hostname.split('.');

	if ( ! reserved_TLDs.includes( query_domain[query_domain.length - 1] ) )
	{
		// Input does not belong to a reserved TLD

		if (
			2 < query_domain.length
			&& query_domain[0] === 'www'
		)
		{
			// Input domain is at least third-level, begins with "www"

			// Remove first 4 characters from hostname ("www.")
			query_url.hostname = query_url.hostname.slice(4);

			// Append
			query_list.push(query_url);
			return query_list.concat(
				TreeOfResponsibility( query_url, respSeq )
			);
		}
		else
		{
			if (
				2 <= query_domain.length
				&& query_domain[0] != 'www'
			)
			{
				// Input domain is at least second-level, does not begin with "www"

				// Prepend "www." to hostname
				query_url.hostname = 'www.' + query_url.hostname;

				// Append
				query_list.push(query_url);
				return query_list.concat(
					TreeOfResponsibility( query_url, respSeq )
				);
			}
			else
			{
				// Skip short, non-www, domains
				return query_list;
			}
		}
	}
	else
	{
		// Input belongs to a reserved TLD

		// Skip reserved TLDs
		return query_list;
	}
}

function alternativePath(url, respSeq) {
	const query_list = new Array();
	const query_url  = new URL(url);

	const query_url_last_sep = url.pathname.lastIndexOf('/');

	if (
		0 <= query_url_last_sep
		&& query_url_last_sep < (url.pathname.length - 1)
	)
	{
		// Input path does not end in '/', or is just '/'

		if (
			index_names.includes(
				query_url.pathname.slice(query_url_last_sep + 1)
			)
		)
		{
			// Input path ends in known "index" file

			// Remove "index" file from path
			query_url.pathname = query_url.pathname.slice(0, query_url_last_sep + 1);

			// Append
			query_list.push(query_url);
			return query_list.concat(
				TreeOfResponsibility( query_url, respSeq )
			);
		}
		else
		{
			// Input path does not end in known "index" file

			// Skip
			return query_list;
		}
	}
	else
	{
		// Input path ends in '/', but is not '/'

		if ( 0 < query_url_last_sep )
		{
			// Input path is not '/'

			// Remove final '/' from path
			query_url.pathname = query_url.pathname.slice(0, query_url_last_sep);

			// Append
			query_list.push(query_url);
			return query_list.concat(
				TreeOfResponsibility( query_url, respSeq )
			);
		}
		else
		{
			// Input path is '/'

			// Skip
			return query_list;
		}
	}
}

function alternativeHash(url, respSeq) {
	const query_list = new Array();
	const query_url  = new URL(url);

	if (url.hash !== '')
	{
		// Input contains a valid hash

		// Clear hash
		query_url.hash = '';

		// Append
		query_list.push(query_url);
		return query_list.concat(
			TreeOfResponsibility( query_url, respSeq )
		);
	}
	else
	{
		// Input does not contain a hash

		// Detect "blank" hashes
		if ( url.href.slice(-1) === '#' )
		{
			// Input contains a "blank" hash (ends in "#")

			// Strip last character ("#")
			query_url.href = query_url.href.slice(0, -1);

			// Append
			query_list.push(query_url);
			return query_list.concat(
				TreeOfResponsibility( query_url, respSeq )
			);
		}
		else
		{
			// Input does not contain a "blank" hash (does not end in "#")

			// Append a blank hash ("#")
			query_url.href = query_url.href + '#';

			// Append
			query_list.push(query_url);
			return query_list.concat(
				TreeOfResponsibility( query_url, respSeq )
			);
		}
	}
}

//
// Find functions
//

async function find_original(url) {
	const original_url = new URL(url);
	const original = await FFX_Places.getPlaceInfo(original_url);

	return original;
}

async function find_related(url) {
	const related = new Array();

	// 1. Original URL
	const original_url = new URL(url);

	// 2. Related
	const alternatives = [
		alternativeProtocol,
		alternativeHostname,
		alternativePath,
		alternativeHash
	];
	const search_queue = TreeOfResponsibility( original_url, alternatives );

	if (options_local !== undefined && options_local.debug.log_lookups_count)
	{
		console.log( 'Places lookups: ' + (search_queue.length + 1) );
	}

	if (options_local !== undefined && options_local.debug.log_related_lookups)
	{
		console.log( 'Related lookups for (' + url + ') :' );
		console.dir( search_queue.map( (e) => e.href ) );
	}

	for (let search of search_queue)
	{
		const result = await FFX_Places.getPlaceInfo(search);
		if (0 < result.bookmarks.length)
		{
			related.push( result );
		}
	}
	return related;
}

//
// Refresh functions
//

async function refresh_browser_action(tabId, tabURL) {
	const original = await find_original(tabURL);
	const related = await find_related(tabURL);
	tab_data.set(
		tabId,
		{
			'original': original,
			'related' : related
		}
	);

	// Clear Tooltip
	let tooltip = '';

	// Generate tooltip bookmark info
	if (0 < original.bookmarks.length)
	{
		if (1 === original.bookmarks.length)
		{
			tooltip += tooltip_bookmark_single( original.bookmarks[0] );
		}
		else
		{
			const limit = 3;
			tooltip += original.bookmarks.slice(0,limit).map(tooltip_bookmark_multiple).join('\n');

			if ( limit < original.bookmarks.length )
			{
				tooltip += '\n. . .';
			}
		}
	}
	else
	{
		tooltip += 'Not bookmarked';
	}

	// Generate tooltip related info
	if (0 < related.length)
	{
		tooltip += '\n\nRelated Bookmarks:\n';

		const limit = 3;
		tooltip += related.slice(0,limit).map(tooltip_bookmark_related).join('\n');

		if ( limit < related.length )
		{
			tooltip += '\n. . .';
		}
	}

	// Generate tooltip visits info, only when 1 < visits
	if ( 1 < original.visits.length )
	{
		tooltip += '\n\n' + tooltip_visits(original.visits);
	}

	// Set Button's Title (tooltip)
	browser.browserAction.setTitle(
		{
			'title': tooltip,
			'tabId': tabId
		}
	);

	// Set Button's Icon and Badge
	if (0 < original.bookmarks.length)
	{
		browser.browserAction.setIcon(
			{
				'path': '_images/action.bookmarked.svg',
				'tabId': tabId
			}
		);

		if (1 < original.bookmarks.length)
		{
			if (0 < related.length)
			{
				FFX_UI.browserAction.setBadgeTXT(
					tabId,
					original.bookmarks.length.toString() + '+' + related.length.toString(),
					dup_color,
					null
				);
			}
			else
			{
				FFX_UI.browserAction.setBadgeTXT(
					tabId,
					original.bookmarks.length.toString(),
					dup_color,
					null
				);
			}
		}
		else
		{
			if (0 < related.length)
			{
				FFX_UI.browserAction.setBadgeTXT(
					tabId,
					related.length.toString(),
					alt_color,
					null
				);
			}
			else
			{
				if (1 < original.visits.length)
				{
					FFX_UI.browserAction.setBadgeTXT(
						tabId,
						(original.visits.length - 1).toString(),
						age_color,
						null
					);
				}
				else
				{
					FFX_UI.browserAction.resetBadgeTXT(tabId);
				}
			}
		}
	}
	else
	{
		if (0 < related.length)
		{
			browser.browserAction.setIcon(
				{
					'path'  : '_images/action.related.svg',
					'tabId' : tabId
				}
			);

			FFX_UI.browserAction.setBadgeTXT(
				tabId,
				related.length.toString(),
				alt_color,
				null
			);
		}
		else
		{
			FFX_UI.browserAction.resetIcon(tabId);

			if (1 < original.visits.length)
			{
				FFX_UI.browserAction.setBadgeTXT(
					tabId,
					(original.visits.length - 1).toString(),
					age_color,
					null
				);
			}
			else
			{
				FFX_UI.browserAction.resetBadgeTXT(tabId);
			}
		}
	}
}

function refresh_all_tabs() {
	browser.tabs.query( {} ).then(
		function(tabList) {
			tabList.forEach(
				function(item) {
					refresh_browser_action(item.id, item.url);
				}
			);
		}
	);
}

//
// Event handlers
//

function onUpdatedTab(tabId, changeInfo, tab) {
	if ( changeInfo.status == 'completed' )
	{
		refresh_browser_action(tabId, changeInfo.url);
	}
}

function onUpdatedTab88(tabId, changeInfo, tab) {
	refresh_browser_action(tabId, changeInfo.url);
}


function onRemovedTab(tabId, removeInfo) {
	tab_data.delete(tabId);
}

function onChangedBookmark(id, changeInfo) {
	if ( changeInfo.hasOwnProperty('url') )
	{
		refresh_all_tabs();
	}
	else
	{
		refresh_all_tabs();
	}
}

function onCreatedBookmark(id, bookmark) {
	switch (bookmark.type)
	{
		case 'bookmark':
			refresh_all_tabs();
			break;
		case 'folder':
			refresh_all_tabs();
			break;
		case 'separator':
			break;
		default:
			refresh_all_tabs();
			break;
	}
}

async function onMovedBookmark(id, moveInfo) {
	const place = await FFX_Places.getPlace(id);

	switch (place.type)
	{
		case 'bookmark':
			refresh_all_tabs();
			break;
		case 'folder':
			refresh_all_tabs();
			break;
		case 'separator':
			break;
		default:
			refresh_all_tabs();
			break;
	}
}

function onRemovedBookmark(id, removeInfo) {
	switch (removeInfo.node.type)
	{
		case 'bookmark':
			refresh_all_tabs();
			break;
		case 'folder':
			refresh_all_tabs();
			break;
		case 'separator':
			break;
		default:
			refresh_all_tabs();
			break;
	}
}

function onInstalledRuntime(details) {
	refresh_all_tabs();
}

//
// Initialization
//

load_options();

browser.runtime.getBrowserInfo().then(
	function(info) {
		const majorVer = parseInt( info.version.split('.')[0] );

		// `url` filter only available since Firefox 88
		if ( majorVer < 88 )
		{
			browser.tabs.onUpdated.addListener(onUpdatedTab, { properties: ['status'] } );
		}
		else
		{
			browser.tabs.onUpdated.addListener(onUpdatedTab88, { properties: ['url'] } );
		}
	}
);
browser.tabs.onRemoved.addListener(onRemovedTab);

browser.bookmarks.onChanged.addListener(onChangedBookmark);
browser.bookmarks.onCreated.addListener(onCreatedBookmark);
browser.bookmarks.onMoved.addListener(onMovedBookmark);
browser.bookmarks.onRemoved.addListener(onRemovedBookmark);

browser.runtime.onInstalled.addListener(onInstalledRuntime);

browser.storage.onChanged.addListener(
	async function (changes, areaName) {
		if ('local' === areaName)
		{
			await load_options();
		}
	}
);

refresh_all_tabs();
