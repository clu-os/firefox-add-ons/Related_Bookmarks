// Related Bookmarks v0.3.2
// Copyright © 2024 Boian Berberov
//
// Licensed under the EUPL-1.2 only.
// License text: https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
// SPDX-License-Identifier: EUPL-1.2

'use strict';

let treetop;
let options_local;
let data;

async function load_options() {
	// Load options
	options_local = await browser.storage.local.get(options_default);
	options_local.treemap.fontHeight     = Number(options_local.treemap.fontHeight);
	options_local.treemap.paddingAround  = Number(options_local.treemap.paddingAround);
	options_local.treemap.paddingBetween = Number(options_local.treemap.paddingBetween);
}

async function load_data() {
	// Fetch data from places database
	data = ( await browser.bookmarks.getSubTree(treetop) )[0];
	if ('root________' === treetop)
	{
		data.type = 'folder';
		data.title = 'All Bookmarks';
		data.parent = null;
	}
}

function clear() {
	d3.select('#treemap').selectAll('g').remove();
}

async function render() {

	// Get current document dimensions
	var width = document.documentElement.clientWidth;
	var height = document.documentElement.clientHeight;
	// var width = document.body.offsetWidth();
	// var height = document.body.offsetHeight();

	// Augment data with hierarchy information
	const hdata = d3.hierarchy(data)
		.sum(
			d => (
				'folder' === d.type
				?
				d.children.filter( e => 'bookmark' === e.type ).length + 1
				:
				0
			)
		)
	;

	// Treemap ordering
	switch(options_local.treemap.sort)
	{
		case 'natural':
			hdata.sort( (a, b) => (a.data.index - b.data.index) )
			break;
		case 'size':
			hdata.sort( (a, b) => (b.value - a.value) )
			break;
		case 'name':
			hdata.sort(
				(a, b) => (
					'folder' === a.data.type
					?
						'folder' === b.data.type
						?
						a.data.title.localeCompare(b.data.title)
						:
						-1
					:
						'folder' === b.data.type
						?
						1
						:
						b.value - a.value
				)
			);
			break;
	}

// Color template
	const colorize = d3.scaleOrdinal( d3[options_local.treemap.colors] );

	// Create TreeMap generator
	const tmap = d3.treemap()
		.tile( d3[options_local.treemap.tiling] )
		.size( [width, height] )
		.paddingOuter(options_local.treemap.paddingAround)
		.paddingInner(options_local.treemap.paddingBetween)
		.paddingTop(options_local.treemap.fontHeight + 2 * options_local.treemap.paddingAround)
	;

	// Create TreeMap and return its root element
	var root = tmap(hdata);

	// Update SVG element
	const svg = d3.select('#treemap')
		.attr('viewBox', [0, 0, width, height])
		.style('font', options_local.treemap.fontHeight + 'px sans-serif');

	// Create a selection `node` and join with decedents
	const node = svg.selectAll('g')
		.data( root.descendants() )
		.join('g')
	;

	// Select the folders
	const folders = node.filter(
		(d, i) => 'folder' === d.data.type
	);

	// Add tooltips
	folders.append('title')
			.text(
				d => (
					d.data.title
					+ '\nTotal: ' + ( d.value - 1 )
					+ '\nLocal: ' + d.data.children.filter( e => 'bookmark' === e.type ).length
					+ '\nBox Size: ' + (d.x1 - d.x0).toFixed(1) + ' x ' + (d.y1 - d.y0).toFixed(1)
				)
			)
	;

	// Add rectangles for folders
	folders.append('rect')
			.attr('fill', d => colorize(d.depth) )
			.attr('x', d => d.x0)
			.attr('y', d => d.y0)
			.attr('width',  d => d.x1 - d.x0)
			.attr('height', d => d.y1 - d.y0)
			.attr('id', d => ('rect-' + d.data.id) )
	;

	// Select folders big enough for titles
	const folder_titles = folders.filter(
		(d, i) => (
			(options_local.treemap.fontHeight + 2 * options_local.treemap.paddingAround) < (d.y1 - d.y0)
			&&
			(options_local.treemap.fontHeight * 2 + 2 * options_local.treemap.paddingAround) < (d.x1 - d.x0)
		)
	);

	// Add clip paths for titles
	folder_titles.append('clipPath')
			.attr('id', d => ('clip-' + d.data.id) )
			.append('rect')
				.attr('x', d => d.x0 + options_local.treemap.paddingAround)
				.attr('y', d => d.y0 + options_local.treemap.paddingAround)
				.attr('width',  d => d.x1 - d.x0 - 2 * options_local.treemap.paddingAround)
				.attr('height', d => options_local.treemap.fontHeight)
	;
			// .append('use')
			// 	.attr('xlink:href', d => ('#rect-' + d.data.id) )

	// Add titles
	folder_titles.append('text')
			.attr('x', d => d.x0 + options_local.treemap.paddingAround)
			.attr('y', d => d.y0 + options_local.treemap.fontHeight + options_local.treemap.paddingAround)
			.attr('cursor', 'pointer')
			.attr('clip-path', d => ('url(#clip-' + d.data.id + ')') )
			.text(d => d.data.title)
			.on(
				'click',
				async function(event) {
					if (event.target.__data__.data.id !== 'root________')
					{
						if (event.target.__data__.data.id === treetop)
						{
							treetop = event.target.__data__.data.parentId;
						}
						else
						{
							treetop = event.target.__data__.data.id;
						}
						clear();
						await load_data();
						await render();
					}
				}
			)
	;

	svg.append( node );
}

window.addEventListener(
	'load',
	async function (event) {
		await load_options();
		treetop = options_local.treemap.treetop;
		await load_data();
		await render();
	}
);

browser.storage.onChanged.addListener(
	async function (changes, areaName) {
		if ('local' === areaName)
		{
			await load_options();
			clear();
			await render();
		}
	}
);
