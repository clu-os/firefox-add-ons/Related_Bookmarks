# Related Bookmarks

> Indicates bookmaked pages, bookmarks with similar URLs, duplicate bookmarks, and history information about the current tab.  Experimental treemap view of all bookmarks.

Displays information about bookmarks as you browse:

- Bookmark details
	- Bookmarked title (_not current page title_)
	- Addition/Creation date
	- Location in bookmarks hierarchy
	- Last visited date
	- Number of visits
- _Duplicate_ bookmarks, when matched on exact URL
	- Location in bookmarks hierarchy
- _Related_ bookmarks, with these possible URL variations
	- Different protocols (`http`, `https`)
	- Extra/Missing `www.` hostname prefix
		- at third or higher domain name level
		- excluding some [reserved TLDs](https://en.wikipedia.org/wiki/Top-level_domain#Reserved_domains)
	- Missing trailing path slashes (`/`)
	- Missing hashes (`#`)
	- Extra "blank" hashes (`#`)
	- Missing "index" pages (`index.html`, etc.)

To Do:

- Additional matching _Related_ bookmarks, with these URL variations
	- Extra trailing path slashes (`/`)
	- Extra "index" pages (`index.html`, etc.)
	- Path elements capitalization, site specific:
		- github.com
	- Extra/Missing trailing/inline path "language" element (`/en/`, `/en-US/`, etc.) and capitalization differences

This extension is limited by the following Firefox bugs:

- [1352835 - bookmarks.search fails on non-http(s) URLs](https://bugzilla.mozilla.org/show_bug.cgi?id=1352835)

Experimental [treemap](https://en.wikipedia.org/wiki/Treemapping) view of all bookmarks, accessible from the Options/Preferences

## Install

Install from [addons.mozilla.org](https://addons.mozilla.org/en-US/firefox/addon/related_bookmarks/)

## License

Licensed under the [EUPL-1.2](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12) only.

- [D3](https://d3js.org/)
	- Copyright © 2010–2024 Mike Bostoc
	- Licensed under the [ISC License](https://spdx.org/licenses/ISC.html)
