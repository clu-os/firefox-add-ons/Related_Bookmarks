// Related Bookmarks v0.3.2
// Copyright © 2024 Boian Berberov
//
// Licensed under the EUPL-1.2 only.
// License text: https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
// SPDX-License-Identifier: EUPL-1.2

'use strict';

const options_default = {
	debug : {
		log_lookups_count   : false,
		log_related_lookups : false,
	}
	,
	treemap : {
		treetop : 'root________',
		colors  : 'schemeCategory10',
		tiling  : 'treemapBinary',
		sort    : 'natural',
		fontHeight     : 10,
		paddingAround  : 4,
		paddingBetween : 2,
	}
}
