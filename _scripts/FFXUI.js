// Firefox Extensions UI utilities v0.1.0
// Copyright © 2020, 2021, 2022 Boian Berberov
//
// Licensed under the EUPL-1.2 only.
// License text: https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
// SPDX-License-Identifier: EUPL-1.2

const FFX_UI = {
	'browserAction' : {
		'resetIcon' : function(tabId) {
			browser.browserAction.setIcon(
				{ 'path': null, 'tabId': tabId }
			);
		}
		,
		'resetBadgeTXT' : function(tabId) {
			this.setBadgeTXT(tabId, null, null, null);
		}
		,
		'setBadgeTXT' : function(tabId, text, bgColor, fgColor) {
			/**
			* @param tabId   
			* @param text    
			* @param bgColor 
			* @param fgColor 
			**/
			if (bgColor !== undefined)
			{
				browser.browserAction.setBadgeBackgroundColor(
					{ 'color' : bgColor, 'tabId' : tabId }
				);
			}
			if (fgColor !== undefined)
			{
				browser.browserAction.setBadgeTextColor(
					{ 'color' : fgColor, 'tabId' : tabId }
				);
			}

			if ( text === null || text.length <= 3 )
			{
				browser.browserAction.setBadgeText(
					{ 'text' : text,  'tabId' : tabId }
				);
			}
			else
			{
				browser.browserAction.setBadgeText(
					{ 'text' : '...', 'tabId' : tabId }
				);
			}
		}
	}
}
