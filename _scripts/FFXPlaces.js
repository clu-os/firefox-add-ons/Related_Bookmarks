// Firefox Extensions Places utilities v0.2.0
// Copyright © 2020, 2021, 2022, 2024 Boian Berberov
//
// Licensed under the EUPL-1.2 only.
// License text: https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
// SPDX-License-Identifier: EUPL-1.2

const FFX_Places = {
	'getPath' : async function (bookmark) {
		/**
		 * @param bookmark of type bookmarks.BookmarkTreeNode
		 **/
		const path = new Array();

		let curParentId = bookmark.parentId;
		while (curParentId !== 'root________')
		{
			let parent = await this.getPlace(curParentId);
			path.unshift(parent);
			curParentId = parent.parentId;
		}

		return path;
	}
	,
	'getPathTitles' : async function (bookmark) {
		/**
		 * @param bookmark of type bookmarks.BookmarkTreeNode
		 **/
		const path = new Array();

		let curParentId = bookmark.parentId;
		while (curParentId !== 'root________')
		{
			let parent = await this.getPlace(curParentId);
			path.unshift(parent.title);
			curParentId = parent.parentId;
		}

		return path;
	}
	,
	'getPathIds' : async function (bookmark) {
		/**
		 * @param bookmark of type bookmarks.BookmarkTreeNode
		 **/
		const path = new Array();

		let curParentId = bookmark.parentId;
		while (curParentId !== 'root________')
		{
			let parent = await this.getPlace(curParentId);
			path.unshift(parent.id);
			curParentId = parent.parentId;
		}

		return path;
	}
	,
	'getVisits' : async function(url) {
		return (
			await browser.history.getVisits( {'url' : url} )
		).filter(
			function(element) {
				return element.transition !== 'reload';
			}
		);
	}
	,
	'getBookmarks' : async function(url) {
		/**
		 * @param url of type URL
		 **/
		const bookmark_data = new Object();
		bookmark_data['url'] = url;

		// BUG: https://bugzilla.mozilla.org/show_bug.cgi?id=1352835
		switch (url.protocol)
		{
			case 'http:':
			case 'https:':
				bookmark_data['bookmarks'] = await browser.bookmarks.search( {'url' : url.href} );
				break;
			case 'about:':
			case 'file:':
				bookmark_data['bookmarks'] = (
					await browser.bookmarks.search(url.href)
				).filter( (e) => e.url === url.href );
				break;
			default:
				bookmark_data['bookmarks'] = new Array();
		}

		if (0 < bookmark_data.bookmarks.length)
		{
			// Multiple for bookmark
			for (let bookmark of bookmark_data.bookmarks)
			{
				bookmark['path'] = await this.getPath(bookmark);
			}
		}

		return bookmark_data;
	}
	,
	'getPlaceInfo' : async function(url) {
		/**
		 * @param url of type URL
		 **/
		const place = new Object();
		place['url'] = url;

		// BUG: https://bugzilla.mozilla.org/show_bug.cgi?id=1352835
		switch (url.protocol)
		{
			case 'http:':
			case 'https:':
				place['bookmarks'] = await browser.bookmarks.search( {'url' : url.href} );
				break;
			case 'about:':
			case 'file:':
				place['bookmarks'] = (
					await browser.bookmarks.search(url.href)
				).filter( (e) => e.url === url.href );
				break;
			default:
				place['bookmarks'] = new Array();
		}

		if (0 < place.bookmarks.length)
		{
			// Unique for place
			place['visits'] = await this.getVisits(url.href);

			// Multiple for bookmark
			for (let bookmark of place.bookmarks)
			{
				bookmark['path'] = await this.getPath(bookmark);
			}
		}
		else
		{
			place['visits'] = new Array();
		}

		return place;
	}
	,
	'getPlace' : async function(id) {
		/**
		 * @param id is the identifier of the place
		 **/
		return ( await browser.bookmarks.get(id) )[0];
	}
}
